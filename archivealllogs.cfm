<cfscript>
/**
 * Archives all log files in CFADMIN.  Could be used in a scheduled maintenance task.  File size(s) returned should usually be less than 200 B if archived successfully.
 * v1.0 by Chris Simmons
 *
 * @param adminpassword		CFADMIN password. (Required)
 * @param adminuser			CFADMIN adminuser. (Optional)
 * @return					Returns struct of log file(s) and theirs size(s).
 * @author					Chris Simmons (ccsimmons@gmail.com)
 * @version					1.0, May 28, 2014
 */
 // usage: result = archiveAllLogs("!TopS3cr3T");
struct function archiveAllLogs(required string adminpassword, string adminuser="admin"){

    var resultObj = structNew();
    var logFileSize = "";
    var adminObj = createObject("component","cfide.adminapi.administrator").login(arguments.adminpassword,arguments.adminuser);
    var serverObj = createObject("component","cfide.adminapi._servermanager.servermanager");

    // loop through log files.
    for (f in serverObj.getLogFiles()){
    	try {
			// archive log file(s)
    		serverObj.archiveLogFile(f);
    		logFileSize = Len(toString(serverObj.viewLogFile(f))) & "B";
			// success.
    		structInsert(resultObj, f, logFileSize);
		} catch (any e) {
			// fail.
    		structInsert(resultObj, f, "fail");
		}
    }

    return resultObj;
}
</cfscript>
