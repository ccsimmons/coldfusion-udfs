# coldfusion-udfs

By [ccsimmons](http://www.cfchimp.com/).

## Description
A collection of udf(s) for ColdFusion.

## Installation

1. Create a folder somewhere on your ColdFusion server
2. Put the contents of this repo in it (minus the LICENSE and README)
3. Create a mapping to the folder in CFADMIN.

**OR**

1. Put the contents of this folder (minus the LICENSE and README) in the directory on your ColdFusion server that you currently use for udfs.

## Usage

There are usage instructions within each udf.

## Configuration

There _may_ be configuration instructions in each udf if necessary.

## Information

I currently use ColdFusion 10 so these udfs are only tested on CF10.

### Known Issues

If you discover any bugs, feel free to create an issue on the [Bitbucket Issue Tracker](https://bitbucket.org/ccsimmons/coldfusion-udfs/issues).

## Authors

* [Chris Simmons](mailto:ccsimmons@gmail.com)

## License

[DBAD](LICENSE)
